package com.gmail.il.krasilnikov

import kotlin.math.sqrt

// так как среди операций присутствует деление, метод applyOperation должен будет вернуть Double, иначе 10/3 будет равен 3
// чтобы иметь возможность присваивать результат вызова метода applyOperation переменной типа State, нужно, чтобы класс State имел поле типа Double

data class State(val value: Double)

sealed class Operation {
    class Multiply(val value: Double) : Operation()
    class Division(val value: Double) : Operation()
    class Addition(val value: Double) : Operation()
    class Subtraction(val value: Double) : Operation()
    object Sqrt : Operation() // чтобы оправдаться почему поле value относится не к самому Operation()
    object BecomeJuniorWhenYouAreTeamLead : Operation()
}

fun State.applyOperation(op: Operation): State {
    val result = when (op) {
        is Operation.Multiply -> value * op.value
        is Operation.Addition -> value + op.value
        is Operation.Division -> value / op.value
        is Operation.Subtraction -> value - op.value
        is Operation.Sqrt -> sqrt(value)
        is Operation.BecomeJuniorWhenYouAreTeamLead -> {
            throw IllegalArgumentException("Ingeniously")
        }
    }
    return if (result != value)
        State(result)
    else
        this
}


fun main() {
    val multiplyByOne = Operation.Multiply(1.0)
    val multiplyByTwo = Operation.Multiply(2.0)

    var initial = State(5.0)
    println(initial)

    initial = initial.applyOperation(multiplyByTwo)
    println(initial)

    println(initial.applyOperation(Operation.Sqrt))

    try {
        initial.applyOperation(Operation.BecomeJuniorWhenYouAreTeamLead)
    } catch (e: Exception) {
        println(e.message)
    }

}